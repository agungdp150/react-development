import React, { Component } from 'react'

// Import component
import ReduxComp from '../Redux Component/ReduxComp';
import TodoList from '../Redux Component/TodoList';


class Homepage extends Component {
    render() {
        return (
            <div>
                <ReduxComp />
                <TodoList />
            </div>
        )
    }
}

export default Homepage;
