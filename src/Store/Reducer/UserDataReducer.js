import {USER_DATA} from '../types';

const initialState = {
    allUser: []
}


// eslint-disable-next-line import/no-anonymous-default-export
export default function(state = initialState, action) {
    const { type, payload } = action;

    switch(type) {
        case USER_DATA: 
        return {
            ...state,
            ...payload,
            allUser: payload
        };
        default:
            return state;
    }
}