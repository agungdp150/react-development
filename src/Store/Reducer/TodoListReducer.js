import {TODO_LIST, ADD_LIST} from '../types';

const initialState = {
    listAll: [],
    addList: {}
}


// eslint-disable-next-line import/no-anonymous-default-export
export default function(state = initialState, action) {
    const { type, payload } = action;

    switch(type) {
        case TODO_LIST: 
            return {
                ...state,
                ...payload,
                listAll: payload
        };
        case ADD_LIST:
            return {
                ...state,
                ...payload,
                addList: payload
            }
        default:
            return state;
    }
}