import {ADD_LIST} from '../types'

const initialState = {
    title: ""
}


export default function (state = initialState, action) {
    const {type, payload} = action

    switch(type) {
        case ADD_LIST:
            return {
                ...state,
                ...payload,
                title: payload
        };
        default: 
            return state
    }
}