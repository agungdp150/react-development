import {combineReducers} from 'redux';


// Import component reducer
import GetDataUser from "./UserDataReducer";
import GetTodoData from "./TodoListReducer";
// import AddDataList from "./AddDataReducer";

export default combineReducers({
    getDataUserAll: GetDataUser,
    getDataListAll: GetTodoData,
    // addData: AddDataList
})