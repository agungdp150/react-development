import axios from 'axios';
import {USER_DATA, TODO_LIST} from '../types';

export const getDataUser = () => async (dispatch) => {
    try {
        const response = await axios.get('https://jsonplaceholder.typicode.com/users');
        dispatch({
            type: USER_DATA,
            payload: response.data
        })
    } catch(error) {
        console.log(error)
    }
}


export const getDataList = () => async (dispatch) => {
    try {
        const response = await axios.get('https://jsonplaceholder.typicode.com/todos?_limit=10');
        dispatch({
            type: TODO_LIST,
            payload: response.data
        })
    } catch(error) {
        console.log(error)
    }
}

