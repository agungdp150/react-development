import axios from 'axios';
import {ADD_LIST} from '../types';


export const addListTodo = addList => async dispatch => {
    console.log("add list yes")
    try {
        const response = await axios.post('https://jsonplaceholder.typicode.com/todos', addList);
        dispatch({
            type: ADD_LIST,
            payload: response.data
        })
    } catch (error) {
        console.log(error)
    }
}