import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import rootReducer from './Reducer/index';

const tengah = [thunk];


const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(...tengah)),
    // applyMidleware(...tengah)
)


export default store;