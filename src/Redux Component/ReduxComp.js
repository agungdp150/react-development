import React, { Component } from 'react'
import {connect} from 'react-redux'
import {getDataUser} from '../Store/Action/UserData'

class ReduxComp extends Component {

    constructor(props){
        super(props);

        this.state = {}
    };

    componentDidMount() {
        this.props.getDataUser();
    }



    render() {
        const allData = this.props.allUser.map((user, i) => {
            return(
                <div key={user.id}>
                    <b>Name: {user.name}</b>
                    <br />
                    <b>Phone: {user.phone}</b>
                    <br/>
                    <br/>
                </div>
            )
        })

        return (
            <div>
                <h1>Hello From Redux Component</h1>
                <hr/>
                {allData}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        allUser: state.getDataUserAll.allUser
    }
}

export default connect(
    mapStateToProps,
    {getDataUser}
)(ReduxComp);
