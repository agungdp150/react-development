import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {getDataList} from '../Store/Action/UserData';
import {addListTodo} from '../Store/Action/PostData';


class TodoList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            title: '',
    };

    this.onChange = this.onChangeValue.bind(this);
    this.onSubmit = this.onSubmitTodo.bind(this);
    }

    componentDidMount() {
        this.props.getDataList();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.addList) {
            this.props.listAll.unshift(nextProps.addList);
        }
    }
        
        onChangeValue = e => {
        this.setState({
            [e.target.name] : e.target.value
        });
    }
    
    
    onSubmitTodo = e => {
        e.preventDefault();

        const post = {
            title: this.state.title,
        };

        this.props.addListTodo(post);
        this.setState({title:  ''});
    }


    render() {

        const data = this.props.listAll
        const allListData = data.map((list, i) => {
            return(
                <div key={i}>
                    <b>List Number: {list.id}</b>
                    <br />
                    <p>Title List: {list.title}</p>
                    <br/>
                    <br/>
                    <br/>
                </div>
            )
        })

        return (
            <div>
                <br/>
                <h1>Hello From Component Todolist</h1>
                <div>
                    <form onSubmit={this.onSubmitTodo}>
                        <input 
                            type="text"
                            name="title"
                            placeholder="Please add your text here"
                            value={this.state.title}
                            onChange={this.onChangeValue}
                        />
                        <button type="submit">
                            Add List
                        </button>
                    </form>
                </div>
                <hr/>
                <div>
                    {allListData}
                </div>
            </div>
        )
    }
}


TodoList.propTypes = {
    getDataList: PropTypes.func.isRequired,
    addListTodo: PropTypes.func.isRequired,
    listAll: PropTypes.array.isRequired,
    addList: PropTypes.object
};

const mapStateToProps = state => ({
    listAll: state.getDataListAll.listAll,
    addList: state.getDataListAll.addList
    });

export default connect(
    mapStateToProps,
    {
        getDataList,
        addListTodo
    }
) (TodoList);


