import React from 'react';
import {Provider} from 'react-redux';


// Redux Component
import RootStore from './Store'

import './App.css';

// Import Page
import Home from "./Page/Homepage.js"

function App() {
  return (
    <div className="App">
      <Provider store={RootStore}>
        <Home />
      </Provider>
    </div>
  );
}

export default App;
